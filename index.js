const express = require('express');
const app = express();
const cors = require('cors');

const port = 3000;

app.use(cors());
app.use(express.json());

let corsOptions = {
  origin: 'http://example.com',
  optionsSuccessStatus: 200,
};

//http://expressjs.com/en/resources/middleware/cors.html
app.get('/', (req, res) => {
  res.send('This is allowed since cors accepts any origin');
});

app.get('/users/:id', cors(corsOptions), (req, res, next) => {
  res.json({ msg: 'This is CORS-enabled for only example.com.' });
});

app.listen(port, () => {
  console.log(`Server running on port: http://localhost:${port}`);
});
